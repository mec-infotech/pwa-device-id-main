import React, { useEffect, useState } from "react";
import {
  isMobile,
  isTablet,
  browserName,
  osName,
  isIPad13,
} from "react-device-detect"; 
import FingerprintJS from '@fingerprintjs/fingerprintjs';
import { getFingerprint } from '@thumbmarkjs/thumbmarkjs'
import { SafeAreaView, StatusBar, View, Text } from "react-native";
 
export const Login = () => {
  const [fingerprint, setFingerprint] = useState('');
  const [deviceInfo, setDeviceInfo] = useState({
    userAgent: navigator.userAgent,
    screenDimensions: `${window.screen.width}x${window.screen.height}`,
    deviceId: "",
    ipAddress: null as string | null,
    isIPad: isIPad13,
    isMobile: isMobile,
    isTablet: isTablet,
    browser: browserName,
    operatingSystem: osName,
  });
 
  useEffect(() => {
    const fpPromise = FingerprintJS.load();
    const getDeviceId = async (): Promise<string> => {
      try {
        const fp = await fpPromise;
        const result = await fp.get();
        console.log("Result: ", result);
        const visitorId = result.visitorId;
        console.log('Visitor ID:', visitorId);
        return visitorId;
      } catch (error) {
        console.error('Error obtaining fingerprint:', error);
        return 'Unavailable';
      }
    };
 
    const fetchIpAddress = async () => {
      try {
        const response = await fetch("https://api.ipify.org/?format=json");
        const data = await response.json();
        const fingerprintResult = await getFingerprint();
        const deviceId = typeof fingerprintResult === 'string' ? fingerprintResult : fingerprintResult.hash;
 
        console.log("Fingerprint result: ", fingerprintResult);
 
        const updatedDeviceInfo = {
          ...deviceInfo,
          deviceId: deviceId,
          ipAddress: data.ip,
        };
        console.log("Updated device info: ", updatedDeviceInfo);
        setDeviceInfo(updatedDeviceInfo);
 
      } catch (error) {
        console.error("Failed to fetch IP address:", error);
        const updatedDeviceInfo = {
          ...deviceInfo,
          ipAddress: "Unavailable",
        };
        const deviceId = await getDeviceId();
        updatedDeviceInfo.deviceId = deviceId;
        setDeviceInfo(updatedDeviceInfo);
      }
    };
 
    fetchIpAddress();
  }, []);

  return (
      <SafeAreaView>
        <StatusBar barStyle="dark-content" />


        <View style={{marginTop: 300, justifyContent:"center", alignItems: "center"}}>
          <View>
            <Text>Device Information</Text>
            <Text>User Agent: {deviceInfo.userAgent}</Text>
            <Text>Screen Dimensions: {deviceInfo.screenDimensions}</Text>
            <Text>Device ID: {deviceInfo.deviceId}</Text>
            <Text>IP Address: {deviceInfo.ipAddress}</Text>
            <Text>Is iPad: {deviceInfo.isIPad ? "Yes" : "No"}</Text>
            <Text>Is Mobile: {deviceInfo.isMobile ? "Yes" : "No"}</Text>
            <Text>Is Tablet: {deviceInfo.isTablet ? "Yes" : "No"}</Text>
            <Text>Browser: {deviceInfo.browser}</Text>
            <Text>Operating System: {deviceInfo.operatingSystem}</Text>
          </View>

        </View>
      </SafeAreaView>
  );
};
 
 